#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Python ACME RPC Daemon
'''
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
import socketserver
import subprocess

PYACMED_VERSION = "0.1"

def run_cmd(cmd):
    try:
        return subprocess.Popen(cmd, \
                                        stdout = subprocess.PIPE, \
                                        stderr= subprocess.PIPE).communicate()
    except:
        return False

class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/acme',)

class SimpleThreadedXMLRPCServer(socketserver.ThreadingMixIn, SimpleXMLRPCServer):
        pass

# Create server
server = SimpleThreadedXMLRPCServer(("0.0.0.0", 8000),
                                    requestHandler=RequestHandler)
server.register_introspection_functions()

# Info function, get ACME info string
def version():
	return PYACMED_VERSION
server.register_function(version)

# Dump the probe configuration
def info(probe):
	if int(probe) < 1:
		return "Please provide a probe number >= 1"
	device = (int(probe) - 1) % 8
	bus = ((int(probe) - 1) / 8) + 1
	(txt, err) = run_cmd(["dut-dump-probe", "-b", "%d" % bus, "%d" % (device + 1)])
	if txt:
		return txt.decode("utf-8").rstrip()
	else:
		return "Failed (%s)" % err.decode("utf-8").rstrip()
server.register_function(info)

# Switch Probe On, takes <probe> id as argument
def switch_on(probe):
	if int(probe) < 1:
		return "Please provide a probe number >= 1"
	(txt, err) = run_cmd(["dut-switch-on", probe])
	if not err:
		return "Success"
	else:
		return "Failed (%s)" % err.decode("utf-8").rstrip()
server.register_function(switch_on)

# Switch Probe Off, takes <probe> id as argument
def switch_off(probe):
	if int(probe) < 1:
		return "Please provide a probe number >= 1"
	(txt, err) = run_cmd(["dut-switch-off", probe])
	if not err:
		return "Success"
	else:
		return "Failed (%s)" % err.decode("utf-8").rstrip()
server.register_function(switch_off)

# Rename the ACME hostname device, takes <new_hostname> as argument, this command will force the reboot of the board
def rename(cape_name):
	with open(" /etc/hostname", "w") as f:
		f.write("%s" % cape_name)
	return "Renaming successful.\nRebooting ACME cape for changes to take effect, please wait ..."
	run_cmd(["reboot"])
server.register_function(rename)

# Provides different ACME informations : the release, connected probes, available slots, ...
def system_info():
	(txt, err) = run_cmd(["system-info"])
	if txt:
		return txt.decode("utf-8").rstrip()
	else:
		return "Failed (%s)" % err.decode("utf-8").rstrip()
server.register_function(system_info)

# System software reboot
def system_reboot():
	run_cmd(["reboot"])
server.register_function(system_reboot)

# Run the server's main loop
server.serve_forever()
